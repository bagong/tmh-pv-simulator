#!/usr/bin/env python3

import datetime
import logging
import random
import sys
from time import sleep

import pika
from dateutil import tz


def get_utc_timestamp() -> int:
    return int(datetime.datetime.now(tz=tz.tzutc()).timestamp())


def main():
    logging.basicConfig(level=logging.INFO)
    logging.info("Initializing mock meter")

    rabbitmq_host = "localhost"

    if len(sys.argv) == 2:
        rabbitmq_host = sys.argv[1]

    connection = pika.BlockingConnection(pika.ConnectionParameters(rabbitmq_host))
    channel = connection.channel()

    channel.exchange_declare(exchange="pv_exchange", exchange_type="direct")

    try:
        while True:
            sleep(0.5)
            val = random.randint(0, 10)
            logging.info("sending val=%d", val)

            channel.basic_publish(
                exchange="pv_exchange",
                routing_key="meter_data",
                body=str(val),
                properties=pika.BasicProperties(timestamp=get_utc_timestamp()),
            )
    except KeyboardInterrupt:
        logging.info("Ending mock meter gracefully..")
        connection.close()


if __name__ == "__main__":
    main()
