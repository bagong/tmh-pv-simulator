#!/usr/bin/env python3
"""
Small script to generate mock power consumption. Using simple bell curve generator.
"""
import datetime

import matplotlib.pyplot as plt  # type: ignore
import numpy as np
from scipy.stats import norm  # type: ignore

PEAK_HOUR = 14  # Peak consumption is at ca. 14:00
PEAK_CONSUMPTION_W = 9000
BELL_CURVE_SCALE = 11000  # arbitrary scale to adjust power consumption curve

# Get number of seconds in a day
ts_0 = datetime.datetime.timestamp(datetime.datetime(2020, 1, 1))
ts_1 = datetime.datetime.timestamp(datetime.datetime(2020, 1, 2))
SECONDS_IN_A_DAY = int(ts_1 - ts_0)
assert SECONDS_IN_A_DAY > 0

PEAK_HOUR_TS = int(PEAK_HOUR / 24.0 * SECONDS_IN_A_DAY)

# Generate x values
x = np.arange(0, SECONDS_IN_A_DAY, 1)
x_human = [datetime.datetime.fromtimestamp(ts).strftime("%H:%M") for ts in x]

y = norm.pdf(x, loc=PEAK_HOUR_TS, scale=BELL_CURVE_SCALE)

# Scale y-values
y = y * (PEAK_CONSUMPTION_W / y.max())

# Add some noise
y = y + np.random.randint(10, 100, size=len(x))

print("plotting")
plt.style.use("seaborn")

plt.plot(x, y)
plt.ylabel("Power consumption (Watts)")
plt.xlabel("timestamp (seconds since Unix Epoch)")

# Markers at 7:00 and 20:00
marker_1 = int(7 / 24 * SECONDS_IN_A_DAY)
marker_2 = int(20 / 24 * SECONDS_IN_A_DAY)
plt.axvline(x=marker_1)
plt.text(marker_1 + 1, 0, "07:00")
plt.axvline(x=marker_2)
plt.text(marker_2 + 1, 0, "20:00")

plt.show()
