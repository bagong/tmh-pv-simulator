#!/usr/bin/env python3
import logging
import sys

import pika


def main():
    logging.basicConfig(level=logging.INFO)
    logging.info("Initializing simple consumer")

    rabbitmq_host = "localhost"
    if len(sys.argv) == 2:
        rabbitmq_host = sys.argv[1]

    connection = pika.BlockingConnection(pika.ConnectionParameters(rabbitmq_host))
    channel = connection.channel()

    channel.exchange_declare(exchange="pv_exchange", exchange_type="direct")

    # create temporary queue
    queue_name = channel.queue_declare(queue="", exclusive=True).method.queue

    channel.queue_bind(
        exchange="pv_exchange", queue=queue_name, routing_key="meter_data"
    )

    def cb(ch, method, properties: pika.spec.BasicProperties, body: bytes):
        meter_value = int(body)
        logging.info("Received %s with %s", str(properties.timestamp), str(meter_value))

        # Very simple PV simulator
        pv_output = meter_value + 10

        channel.basic_publish(
            exchange="pv_exchange", routing_key="pv_output", body=str(pv_output)
        )
        logging.info("Sending PV output of %.2f", pv_output)

    channel.basic_consume(queue=queue_name, on_message_callback=cb, auto_ack=True)

    logging.info("waiting for messages")
    channel.start_consuming()


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print("bye")
        sys.exit(0)
