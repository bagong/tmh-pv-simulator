#!/usr/bin/env python3
import logging
import sys

import pika


def cb(
    ch: pika.channel.Channel,
    method: pika.spec.Basic.Deliver,
    properties: pika.spec.BasicProperties,
    body: bytes,
):
    logging.info(
        "Received from %s: ts %s with %s",
        str(method.routing_key),
        str(properties.timestamp),
        str(body),
    )


def main():
    logging.basicConfig(level=logging.INFO)
    logging.info("Initializing simple consumer")

    rabbitmq_host = "localhost"
    if len(sys.argv) == 2:
        rabbitmq_host = sys.argv[1]

    connection = pika.BlockingConnection(pika.ConnectionParameters(rabbitmq_host))
    channel = connection.channel()

    channel.exchange_declare(exchange="pv_exchange", exchange_type="direct")

    # create temporary queue
    queue_name = channel.queue_declare(queue="", exclusive=True).method.queue

    channel.queue_bind(
        exchange="pv_exchange", queue=queue_name, routing_key="meter_data"
    )

    channel.queue_bind(
        exchange="pv_exchange", queue=queue_name, routing_key="pv_output"
    )
    channel.basic_consume(queue=queue_name, on_message_callback=cb, auto_ack=True)

    logging.info("waiting for messages")
    channel.start_consuming()


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print("bye")
        sys.exit(0)
