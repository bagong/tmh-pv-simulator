# TMH Photovoltaic Simulator

## Requirements

- Docker (tested on v20.10.6)
- docker-compose (tested on v1.28.6)


## Project Structure

This project consist of the following components, each deployed in a docker
container:
- RabbitMQ Development Server (standard official container)
- Mock Meter
- PV Simulator
- Output Logger


## Usage

To run:
```bash
git clone https://gitlab.com/bagong/tmh-pv-simulator
cd tmh-pv-simulator
docker-compose up -d  # this will run all the components described above
```

Output file will be written into local folder `log`:
```bash
cd log
tail -f pv_simulator.log  # to see the file growing
```

To run tests:
```bash
tox
```


## Rationale

### Logging format
Goal: to not drop any data samples.

TODO

### Distribution: Docker Container vs. pip Project
Assuming storage is cheap, and Operations/Deployment problems are expensive, docker
container is the way to go.

### Project Organization
Each Python module is meant to be independent module, stored in separate git
repo. For convenience all modules are contained within this single repo.

To scale up the system, following could be done:
- Split each module/app into its own repo
- Create private pip repository to host common private packages/libraries
- Create orchestration project that spans multiple repo
- etc.

### Resilience of Distributed System
Ideally, each service needs to be able to handle conditions where a needed
service is unreachable or down. There are many ways to do that from the service
side, like [wait-for-it](https://github.com/vishnubob/wait-for-it). In this toy
example, long startup of rabbitmq container creates a race condition for the
applications. It is easier to just simply keep restarting the service when it
fails.


### Simulation Clock
In this toy example, the `mock_meter` module provides the single time source of
the whole system, through the timestamp on the message. The other modules are
state-less (or rather, timeless).

For further development, there should be an independent time source running a
simulated clock. This is particularly useful so that we can have accelerated,
slowed, or stepped control of time over the perceived time of the system. Use
case for this would be e.g. sensor data playback. Here we would want the
(time-dependent) system to correspond accordingly to the simulated time.


## Test Environment
### Setup
```bash
# On fresh installed Ubuntu 20.04 server
sudo apt install docker.io  # Debian upstream package is now up-to-date
sudo apt install python3-pip
sudo pip3 install docker-compose
sudo usermod -aG docker $(whoami)
newgrp docker
```

### Versions
```
$ docker --version
Docker version 20.10.6, build 370c289

$ docker-compose --version
docker-compose version 1.28.6, build unknown

$ uname -r
5.8.0-53-generic

$ cat /etc/lsb-release
DISTRIB_ID=Ubuntu
DISTRIB_RELEASE=20.04
DISTRIB_CODENAME=focal
DISTRIB_DESCRIPTION="Ubuntu 20.04.2 LTS"
```
